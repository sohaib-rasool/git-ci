
file(GLOB SOURCES_app ${CMAKE_SOURCE_DIR}/app/*.cpp)

add_executable(main main.cpp)
target_link_libraries(main git-ci)
